<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="RealmStatus" version="1.6" date="17/8/2009" >

		<Author name="Aiiane" email="aiiane@aiiane.net" />
		<Description text="Gives an overview of the victory points in each pairing for each realm." />
		
        <VersionSettings gameVersion="1.3.1" windowsVersion="1.0" savedVariablesVersion="1.0" />
        
        <Dependencies/>
        
		<Files>
            <File name="LibStub.lua" />
            <File name="LibGUI.lua" />
			<File name="RealmStatus.lua" />
            <File name="RealmStatusClock.lua" />
            <File name="RealmStatus.xml" />
            <File name="RealmStatusHistoryWindow.xml" />
		</Files>
		
		<OnInitialize>
            <CreateWindow name="RealmStatusNotifyAnchor" />
            <CreateWindow name="RealmStatusHistoryWindow" show="false" />
            <CallFunction name="RealmStatus.Initialize" />
		</OnInitialize>
		<OnUpdate>
            <CallFunction name="RealmStatus.OnUpdate" />
            <CallFunction name="RealmStatusClock.OnUpdate" />
        </OnUpdate>
        
        <WARInfo>
    <Categories>
        <Category name="RVR" />
    </Categories>
    <Careers>
        <Career name="BLACKGUARD" />
        <Career name="WITCH_ELF" />
        <Career name="DISCIPLE" />
        <Career name="SORCERER" />
        <Career name="IRON_BREAKER" />
        <Career name="SLAYER" />
        <Career name="RUNE_PRIEST" />
        <Career name="ENGINEER" />
        <Career name="BLACK_ORC" />
        <Career name="CHOPPA" />
        <Career name="SHAMAN" />
        <Career name="SQUIG_HERDER" />
        <Career name="WITCH_HUNTER" />
        <Career name="KNIGHT" />
        <Career name="BRIGHT_WIZARD" />
        <Career name="WARRIOR_PRIEST" />
        <Career name="CHOSEN" />
        <Career name= "MARAUDER" />
        <Career name="ZEALOT" />
        <Career name="MAGUS" />
        <Career name="SWORDMASTER" />
        <Career name="SHADOW_WARRIOR" />
        <Career name="WHITE_LION" />
        <Career name="ARCHMAGE" />
    </Careers>
</WARInfo>

		
	</UiMod>
</ModuleFile>
