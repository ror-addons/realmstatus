RealmStatusClock = {}


local timeCount = 0

local timeRSC = 0
local lastUpdate = nil

RealmStatusClock.visibleMinutes = 10 * 60
RealmStatusClock.delay = 10
RealmStatusClock.MAX_TIME = 24*3600

function RealmStatusClock.GetTime()
	local time = timeRSC
	if time >= RealmStatusClock.MAX_TIME then
		time = time - RealmStatusClock.MAX_TIME
	end
	return time
end

function RealmStatusClock.GetHMS( time )
	if time >= RealmStatusClock.MAX_TIME then
		time = time - RealmStatusClock.MAX_TIME
	end
	
    local h  = math.floor( time / 3600 )
    time = time - h * 3600
    local m = math.floor( time / 60 )
    local s = time - m * 60
    
    return h,m,s
end

function RealmStatusClock.FormatClockDiff( savedTime, timeDiff )
	local text = L""
	local brk = false
	
	if ( timeDiff ) <= ( RealmStatusClock.visibleMinutes + 60 ) then
		if ( timeDiff ) < 60 then
			text = L"<1m ago"
		else
			text = wstring.format(L"%dm ago", math.floor( timeDiff / 60 ))
		end
	else
		text = RealmStatusClock.FormatClock( savedTime )
	end
	
	return text
end       

function RealmStatusClock.FormatClock( time )
    local h,m = RealmStatusClock.GetHMS(time)
    
    local text = L""
    text = wstring.format(L"%02d:%02d", h, m)
    
    return text
end

function RealmStatusClock.OnUpdate(timeElapsed)
	timeRSC = timeRSC + timeElapsed
end

function RealmStatusClock.OnChatText()
	local lastEntry
	lastEntry = TextLogGetNumEntries("Chat") - 1
	if (lastEntry >= 0) then
		lastEntry = TextLogGetEntry("Chat",lastEntry)
	else 
		return
	end
	
	if (lastEntry ~= lastUpdate) then
		lastUpdate = lastEntry
		local h,m,s = lastEntry:match(L"([0-9]+):([0-9]+):([0-9]+)")
		timeRSC = tonumber(h) * 3600 + tonumber(m) * 60 + tonumber(s)
	end
end

function RealmStatusClock.Load()
	local chatLogEventId = TextLogGetUpdateEventId( "Chat" )
    RegisterEventHandler( chatLogEventId,  "RealmStatusClock.OnChatText")
	RealmStatusClock.OnChatText()
end