RealmStatus = {}

local LibGUI = LibStub("LibGUI")
if not LibGUI then return end

local ipairs = ipairs
local tremove = table.remove
local tinsert = table.insert

local firstLoad = true

local timeCount = 0
local historyTimeCount = 0

local MAX_ROWS = 50

-- our window
local W

-- notification box
local WN

-- queue for notifications
local notifyQueue = {}
local notifyTimer = 0

local zoneOrderPercent
local zoneDestroPercent

-- Table for history list
RealmStatus.historyTable = {} 

-- Modes

RealmStatus.campaignMode = 1

ALL_MODE = 1
DVG_MODE = 2
EVC_MODE = 3
HEVDE_MODE = 4

local campaignModes = {
	[ALL_MODE] = "",
	[DVG_MODE] = "DvG",
	[EVC_MODE] = "EvC",
	[HEVDE_MODE] = "HEvDE",
}

-- end modes

local pairingTitle = {
    ["DvG"]=L"Dwarfs vs Greenskins",
    ["EvC"]=L"Empire vs Chaos",
    ["HEvDE"]=L"High Elves vs Dark Elves",
}

local zones = {
    [1] = {
        ["DvG"] = 6,
        ["EvC"] = 106,
        ["HEvDE"] = 206,
    },
    [2] = {
        ["DvG"] = 7,
        ["EvC"] = 107,
        ["HEvDE"] = 207,
    },
    [3] = {
        ["DvG"] = 8,
        ["EvC"] = 108,
        ["HEvDE"] = 208,
    },
    [4] = {
        ["DvG"] = {
            [3] = 3,
            [2] = 5,
            [1] = 9,
        },
        ["EvC"] = {
            [3] = 103,
            [2] = 105,
            [1] = 109,
        },
        ["HEvDE"] = {
            [3] = 203,
            [2] = 205,
            [1] = 209,
        },
    },
}   

local zoneAbbrev = {
    ["DvG"] = {
        [3] = L"Black Crag",
        [2] = L"Thunder Mt.",
        [1] = L"Kadrin Valley",
    },
    ["EvC"] = {
        [3] = L"Chaos Wastes",
        [2] = L"Praag",
        [1] = L"Reikland",
    },
    ["HEvDE"] = {
        [3] = L"Caledor",
        [2] = L"Dragonwake",
        [1] = L"Eataine",
    },
}

local c_ORDER = 1
local c_DESTRO = 2

-- cache the vp values we found in our last update, so we can look for changes
local oldVP = {}
local recentUpdates = {}

local function convertVP(vp)
    if vp == 100 then
        return L"Lock"
    elseif vp == 0 then
        return L"--"
    else
        return wstring.format(L"%d", math.floor(vp * 100 / 66))
    end
end

local function convertDiffVP(vp1, vp2)
    local diff = math.floor(vp1 * 100 / 66) - math.floor(vp2 * 100 / 66)
    if diff > -0.1 and diff < 0.1 then
        return L""
    elseif vp1 == 100 or vp2 == 100 then
        return L""
    else
        if diff > 0 then
            return wstring.format(L"(+%d)", diff)
        else
            return wstring.format(L"(%d)", diff)
        end
    end
end

function RealmStatus.ToggleShowingHistoryWindow()
    if WindowGetShowing( "RealmStatusHistoryWindow" ) == false
		then RealmStatus.ShowHistoryWindow()
	else RealmStatus.HideHistoryWindow()
	end
end

function RealmStatus.ShowHistoryWindow()
    RealmStatus.OnHistoryWindowUpdate()
    WindowSetShowing( "RealmStatusHistoryWindow", true )
end

function RealmStatus.HideHistoryWindow()
    WindowSetShowing( "RealmStatusHistoryWindow", false )
end

function RealmStatus.ToggleShowing()
	if WindowGetShowing("RealmStatusWindow") == false then RealmStatus.Show()
	else W:Hide()
	end
end

function RealmStatus.Show()
    RealmStatus.UpdateVP()
    W:Show()
end

function RealmStatus.OnUpdate(timeElapsed)
    timeCount = timeCount + timeElapsed
    local expiredCount = 0
    for k,v in ipairs(recentUpdates) do
        if v.expireTime <= timeCount then
            v.oRef:Font("font_clear_medium")
            v.dRef:Font("font_clear_medium")
            expiredCount = expiredCount + 1
        else
            break -- these are in chronological order, so if this one isn't expired, none of the next will be either
        end
    end
    for i=1,expiredCount do
        tremove(recentUpdates, 1)
    end
    
    -- Are we ready for the next notification?
    if WN and WN.name then
        if notifyTimer <= timeCount then
            if #notifyQueue > 0 then
                WindowStopAlphaAnimation(WN.name)
                local notify = tremove(notifyQueue, 1)
                notifyTimer = timeCount + RealmStatus.ShowNotify(notify)
            else
                if notifyTimer > -256 then
                    WN:Hide()
                    notifyTimer = -256
                end
            end
        end
    end
end

function RealmStatus.UpdateVP(updatedArea)
    local weCare = false
    for tier=1,3 do
        for k,v in pairs(zones[tier]) do
            if (not updatedArea) or v == updatedArea then
                weCare = true
                cData = GetCampaignZoneData(v)

                local orderVP = convertVP(cData.controlPoints[c_ORDER])
                local destroVP = convertVP(cData.controlPoints[c_DESTRO])
                
                local changed = false
                
                if not oldVP[v] then
                    W[k].tiers[tier].order:Font("font_clear_medium")
                else
                    if cData.controlPoints[c_ORDER] > oldVP[v].controlPoints[c_ORDER] then
                        W[k].tiers[tier].order:Font("font_clear_medium_bold")
                        orderVP = L"+ "..orderVP
                        changed = true
                    elseif cData.controlPoints[c_ORDER] < oldVP[v].controlPoints[c_ORDER] then
                        W[k].tiers[tier].order:Font("font_clear_medium_bold")
                        orderVP = L"- "..orderVP
                        changed = true
                    end
                end
                
                if not oldVP[v] then
                    W[k].tiers[tier].destro:Font("font_clear_medium")
                else
                    if cData.controlPoints[c_DESTRO] > oldVP[v].controlPoints[c_DESTRO] then
                        W[k].tiers[tier].destro:Font("font_clear_medium_bold")
                        destroVP = destroVP..L" +"
                        changed = true
                    elseif cData.controlPoints[c_DESTRO] < oldVP[v].controlPoints[c_DESTRO] then
                        W[k].tiers[tier].destro:Font("font_clear_medium_bold")
                        destroVP = destroVP..L" -"
                        changed = true
                    end
                end
                
                W[k].tiers[tier].order:SetText(orderVP)
                W[k].tiers[tier].destro:SetText(destroVP)

                if changed then
                    local notify = {
                        ["title"] = towstring(k)..L" Tier "..towstring(tier),
                        ["orderVP"] = cData.controlPoints[c_ORDER],
                        ["orderOld"] = oldVP[v].controlPoints[c_ORDER],
                        ["destroVP"] = cData.controlPoints[c_DESTRO],
                        ["destroOld"] = oldVP[v].controlPoints[c_DESTRO],
                    }
                    --For now, we won't show alerts for anything except tier 4. Uncomment this for alerts about tier 1-3 changes.
                    --table.insert(notifyQueue, notify)
                    
                    --[[ T3 History
                    local historyRow = {
						["time"] = RealmStatusClock.GetTime(),
						["visibleTime"]= L"",
						["orderVP"] = convertDiffVP(notify.orderVP, notify.orderOld)..L" "..convertVP(notify.orderVP),
						["location"] = notify.title,
						["destroVP"] = convertVP(notify.destroVP)..L" "..convertDiffVP(notify.destroVP, notify.destroOld),
                    }
                    historyRow.visibleTime = RealmStatusClock.FormatClock( historyRow.time )
                    
                    table.insert(RealmStatus.historyTable, historyRow)
                    
                    RealmStatus.OnHistoryWindowUpdate()
                    --]]
                end
                
                oldVP[v] = cData
                oldVP[v].oRef = W[k].tiers[tier].order
                oldVP[v].dRef = W[k].tiers[tier].destro
            end
        end
    end
    
    for k,v in pairs(zones[4]) do
        for zone=1,3 do
            local zNum = v[zone]
            if (not updatedArea) or zNum == updatedArea then
                weCare = true
                cData = GetCampaignZoneData(zNum)
                
                local orderVP = convertVP(cData.controlPoints[c_ORDER])
                local destroVP = convertVP(cData.controlPoints[c_DESTRO])
                
                local changed = false
                
                if not oldVP[zNum] then
                    W[k].tiers[4].zones[zone].order:Font("font_clear_medium")
                else
                    if cData.controlPoints[c_ORDER] > oldVP[zNum].controlPoints[c_ORDER] then
                        W[k].tiers[4].zones[zone].order:Font("font_clear_medium_bold")
                        orderVP = L"+ "..orderVP
                        changed = true
                    elseif cData.controlPoints[c_ORDER] < oldVP[zNum].controlPoints[c_ORDER] then
                        W[k].tiers[4].zones[zone].order:Font("font_clear_medium_bold")
                        orderVP = L"- "..orderVP
                        changed = true
                    end
                end
                
                if not oldVP[zNum] then
                    W[k].tiers[4].zones[zone].destro:Font("font_clear_medium")
                else
                    if cData.controlPoints[c_DESTRO] > oldVP[zNum].controlPoints[c_DESTRO] then
                        W[k].tiers[4].zones[zone].destro:Font("font_clear_medium_bold")
                        destroVP = destroVP..L" +"
                        changed = true
                    elseif cData.controlPoints[c_DESTRO] < oldVP[zNum].controlPoints[c_DESTRO] then
                        W[k].tiers[4].zones[zone].destro:Font("font_clear_medium_bold")
                        destroVP = destroVP..L" -"
                        changed = true
                    end
                end
                
                W[k].tiers[4].zones[zone].order:SetText(orderVP)
                W[k].tiers[4].zones[zone].destro:SetText(destroVP)

                if changed then
                    local notify = {
                        ["title"] = zoneAbbrev[k][zone],
                        ["orderVP"] = cData.controlPoints[c_ORDER],
                        ["orderOld"] = oldVP[zNum].controlPoints[c_ORDER],
                        ["destroVP"] = cData.controlPoints[c_DESTRO],
                        ["destroOld"] = oldVP[zNum].controlPoints[c_DESTRO],
                    }
                    table.insert(notifyQueue, notify)
                    
                    -- T4 History
                    local historyRow = {
						["time"] = RealmStatusClock.GetTime(),
						["visibleTime"]= L"",
						["orderVP"] = convertDiffVP(notify.orderVP, notify.orderOld)..L" "..convertVP(notify.orderVP),
						["location"] = notify.title,
						["destroVP"] = convertVP(notify.destroVP)..L" "..convertDiffVP(notify.destroVP, notify.destroOld),
                    }
                    historyRow.visibleTime = RealmStatusClock.FormatClock( historyRow.time )
                    
                    table.insert(RealmStatus.historyTable, historyRow)
                    
                    RealmStatus.OnHistoryWindowUpdate()
                end
                oldVP[zNum] = cData
                oldVP[zNum].oRef = W[k].tiers[4].zones[zone].order
                oldVP[zNum].dRef = W[k].tiers[4].zones[zone].destro
            end
        end
    end
    
    -- Update the recentUpdates list if applicable
    if updatedArea and weCare then
        for k,v in ipairs(recentUpdates) do
            if v == oldVP[updatedArea] then
                tremove(recentUpdates, k)
                break
            end
        end
        oldVP[updatedArea].expireTime = timeCount + 60
        tinsert(recentUpdates, oldVP[updatedArea])
    end
    
    -- If the zone updated was our current one, update the hud display. Also update it on zone.
    if not updatedArea or GameData.Player.zone == updatedArea then
        RealmStatus.ShowZoneVP(GameData.Player.zone)
    end
    
end

function RealmStatus.OnSelectCampaignMode()    
    RealmStatus.campaignMode = WindowGetId( SystemData.ActiveWindow.name )
        
    for mode = 1, 4 do
        local pressed = mode == RealmStatus.campaignMode
        ButtonSetPressedFlag("RealmStatusHistoryWindowRadioButtonsCampaignMode"..mode.."Button", pressed )
    end
    RealmStatus.OnHistoryWindowUpdate()
end

function RealmStatus.ShowCampaignMode(newmode)
    RealmStatus.campaignMode = newmode
    
    for mode = 1, 4 do
        local pressed = mode == RealmStatus.campaignMode
        ButtonSetPressedFlag("RealmStatusHistoryWindowRadioButtonsCampaignMode"..mode.."Button", pressed )
    end
    RealmStatus.OnHistoryWindowUpdate()
    RealmStatus.ShowHistoryWindow()
end
    
function RealmStatus.UpdateList()
	local displayOrder = {}
	local campaign = campaignModes[RealmStatus.campaignMode]
	
	if #RealmStatus.historyTable > MAX_ROWS then
		table.remove(RealmStatus.historyTable, 1)
	end
	
	for rowIndex = #RealmStatus.historyTable, 1, -1 do
		if campaign ~= "" then
			local flag = false
			for _, loc in ipairs(zoneAbbrev[campaign]) do
				if RealmStatus.historyTable[rowIndex].location == loc then
					flag = true
					break
				end
			end
			if flag == true then
				table.insert(displayOrder, rowIndex)
			end
		else 
			table.insert(displayOrder, rowIndex)
		end		
	end

	-- This updates ("draws") the listbox.
	ListBoxSetDisplayOrder( "RealmStatusHistoryWindowList", displayOrder )
end

function RealmStatus.OnHistoryWindowUpdate(timeElapsed)
	if timeElapsed == nil then
		historyTimeCount = 0
	else
		historyTimeCount = historyTimeCount - timeElapsed
	end
	
	if historyTimeCount > 0 then
		return end
		
	historyTimeCount = RealmStatus.delay
	
	local curTime = RealmStatusClock.GetTime()
	
	LabelSetText( "RealmStatusHistoryWindowClock", RealmStatusClock.FormatClock( curTime ) )
	
	for index = #RealmStatus.historyTable, 1, -1 do
		local time = RealmStatus.historyTable[index].time
		local text = L""
		
		if time <= curTime then
			text = RealmStatusClock.FormatClockDiff(time, (curTime - time ) )
		else
			text = RealmStatusClock.FormatClockDiff(time, ( curTime + RealmStatusClock.MAX_TIME - time ) )
		end
		
		if text ~= L"" then 
			RealmStatus.historyTable[index].visibleTime = text
		end
	end
	
	RealmStatus.UpdateList()
end

function RealmStatus.ShowZoneVP(zone)
    if not DoesWindowExist("EA_Window_ZoneControl") then return end
    
    if (not zoneOrderPercent) or (not zoneOrderPercent.name) then
        zoneOrderPercent = LibGUI("Label", "RealmStatusOrderZoneVP")
        zoneOrderPercent:Resize(30)
        zoneOrderPercent:Font("font_clear_small_bold")
        zoneOrderPercent:Align("leftcenter")
        zoneOrderPercent:Parent("EA_Window_ZoneControl")
        zoneOrderPercent:Layer(4)
        zoneOrderPercent:AnchorTo("EA_Window_ZoneControlContainer", "left", "left")
        zoneOrderPercent:IgnoreInput()
    end
    
    if (not zoneDestroPercent) or (not zoneDestroPercent.name) then
        zoneDestroPercent = LibGUI("Label", "RealmStatusDestroZoneVP")
        zoneDestroPercent:Resize(30)
        zoneDestroPercent:Font("font_clear_small_bold")
        zoneDestroPercent:Align("rightcenter")
        zoneDestroPercent:Parent("EA_Window_ZoneControl")
        zoneDestroPercent:Layer(4)
        zoneDestroPercent:AnchorTo("EA_Window_ZoneControlContainer", "right", "right")
        zoneDestroPercent:IgnoreInput()
    end
    
    local cData = GetCampaignZoneData(zone)
    if (not cData) or cData.controlPoints[c_ORDER] == 100 or cData.controlPoints[c_DESTRO] == 100 then
        -- If it's locked, don't display numbers. The lock icon should be obvious enough.
        zoneOrderPercent:SetText(L"")
        zoneDestroPercent:SetText(L"")
    else
        zoneOrderPercent:SetText(convertVP(cData.controlPoints[c_ORDER]))
        zoneDestroPercent:SetText(convertVP(cData.controlPoints[c_DESTRO]))
    end
    
end

function RealmStatus.ShowNotify(notice)
    if (not WN) or (not WN.name) then return end
    
    local zoneName = notice.title
    local orderVP = notice.orderVP
    local orderOld = notice.orderOld
    local destroVP = notice.destroVP
    local destroOld = notice.destroOld
    
    local show_time = 10
    local fade_time = 1
    
    WN.Title:SetText(zoneName)
    WN.Order:SetText(convertDiffVP(orderVP, orderOld)..L" "..convertVP(orderVP))
    WN.Destro:SetText(convertVP(destroVP)..L" "..convertDiffVP(destroVP, destroOld))
    
    WindowSetScale(WN.name, WindowGetScale("RealmStatusNotifyAnchor"))
    WindowSetShowing(WN.name, true)
    WindowStartAlphaAnimation(WN.name, Window.AnimationType.EASE_OUT, 1.0, 0.0, fade_time, true, show_time, 0)
    
    -- return the time it will take to display this notification
    return (show_time+fade_time)
end

function RealmStatus.MakeNotify()
    if WN and WN.name then return end
    
    LayoutEditor.RegisterWindow("RealmStatusNotifyAnchor", L"RealmStatus Notification", L"Where RealmStatus notifications are displayed.",
        false, false, true, nil)
    
    -- create notification window
    WN = LibGUI("Blackframe", "RealmStatusNotify")
    WN:Parent("RealmStatusNotifyAnchor")
    WN:AnchorTo("RealmStatusNotifyAnchor", "topleft", "topleft")
    WN:Resize(200, 75)
    WN:IgnoreInput()
    
    WN.Title = WN("Label")
    WN.Title:SetText(L"RealmStatus")
    WN.Title:Resize(180)
    WN.Title:Font("font_clear_medium_bold")
    WN.Title:AnchorTo(WN, "top", "top", 0, 5)
    WN.Title:IgnoreInput()
    
    WN.Order = WN("Label")
    WN.Order:SetText(L"(+00) 000")
    WN.Order:Resize(87)
    WN.Order:Font("font_clear_small_bold")
    WN.Order:Align("rightcenter")
    WN.Order:Color(0, 125, 255)
    WN.Order:AnchorTo(WN.Title, "bottom", "topright", -3)
    WN.Order:IgnoreInput()
    
    WN.Destro = WN("Label")
    WN.Destro:SetText(L"000 (+00)")
    WN.Destro:Resize(87)
    WN.Destro:Font("font_clear_small_bold")
    WN.Destro:Align("leftcenter")
    WN.Destro:Color(255, 0, 0)
    WN.Destro:AnchorTo(WN.Title, "bottom", "topleft", 3)
    WN.Destro:IgnoreInput()
end

function RealmStatus.MakeWin()
    if W and W.name then return end
    
    W = LibGUI("Blackframe", "RealmStatusWindow")
	W:AnchorTo("Root", "center", "center")
    W:Resize(500, 680)
    W:MakeMovable()
    
    W.Title = W("Label")
    W.Title:SetText(L"RealmStatus")
    W.Title:Resize(480)
    W.Title:Font("font_clear_large_bold")
    W.Title:AnchorTo(W, "top", "top", 0, 10)
    W.Title:IgnoreInput()
    
    W.DvG = RealmStatus.MakePairing("DvG")
    W.DvG:AnchorTo(W.Title, "bottom", "top", 0, 5)
    
    W.EvC = RealmStatus.MakePairing("EvC")
    W.EvC:AnchorTo(W.DvG, "bottom", "top", 0, 5)
    
    W.HEvDE = RealmStatus.MakePairing("HEvDE")
    W.HEvDE:AnchorTo(W.EvC, "bottom", "top", 0, 5)
    
    W.Close = W("Closebutton")
    W.Close.OnLButtonUp = function() W:Hide() end
end

function RealmStatus.MakePairing(pairing)
    local e = W("Blackframe")
    e:Resize(480, 205)
    
    if pairing == "DvG" then
        e.OnRButtonUp = function() RealmStatus.ShowCampaignMode(2) end
        e:CaptureInput()
        e:RegisterEvent("OnRButtonUp")
    elseif pairing == "EvC" then
        e.OnRButtonUp = function() RealmStatus.ShowCampaignMode(3) end
        e:CaptureInput()
        e:RegisterEvent("OnRButtonUp")
    elseif pairing == "HEvDE" then
        e.OnRButtonUp = function() RealmStatus.ShowCampaignMode(4) end
        e:CaptureInput()
        e:RegisterEvent("OnRButtonUp")
    end
    
    e.Title = e("Label")
    e.Title:AnchorTo(e, "top", "top", 0, 5)
    e.Title:Resize(460)
    e.Title:SetText(pairingTitle[pairing])
    e.Title:IgnoreInput()
    e.Title:Font("font_clear_medium_bold")
    
    e.tiers = {}
    for i=1,3 do
        e.tiers[i] = RealmStatus.Make3Tier(e, i)
    end
    
    e.tiers[4] = e("Label")
    e.tiers[4]:Resize(460)
    e.tiers[4]:AnchorTo(e.Title, "bottom", "top", 0, 70)
    e.tiers[4]:SetText(L"Tier 4")
    e.tiers[4]:IgnoreInput()
    e.tiers[4]:Font("font_clear_medium")
    
    e.tiers[4].zones = {}
    for i=1,3 do
        e.tiers[4].zones[i] = RealmStatus.Make4Tier(e, pairing, i)
    end
    
    return e
end

function RealmStatus.Make3Tier(e, tier)
    local t
    
    t = e("Label")
    t:SetText(L"Tier "..towstring(tier))
    t:Resize(140)
    t:Position(30+(140*(tier-1)), 42)
    t:IgnoreInput()
    t:Font("font_clear_medium")
    
    t.order = e("Label")
    t.order:SetText(L"100")
    t.order:Resize(65)
    t.order:AnchorTo(t, "bottom", "topright", -5)
    t.order:Color(0, 125, 255)
    t.order:IgnoreInput()
    t.order:Align("rightcenter")
    t.order:Font("font_clear_medium_bold")
    
    t.destro = e("Label")
    t.destro:SetText(L"100")
    t.destro:Resize(65)
    t.destro:AnchorTo(t, "bottom", "topleft", 5)
    t.destro:Color(255, 0, 0)
    t.destro:IgnoreInput()
    t.destro:Align("leftcenter")
    t.destro:Font("font_clear_medium_bold")
    
    return t
end

function RealmStatus.Make4Tier(e, pairing, zone)
    local t
    
    t = e("Label")
    t:SetText(zoneAbbrev[pairing][zone])
    t:Resize(140)
    t:Position(30+(140*(zone-1)), 130)
    t:IgnoreInput()
    t:Font("font_clear_small_bold")
    
    t.order = e("Label")
    t.order:SetText(L"100")
    t.order:Resize(65)
    t.order:AnchorTo(t, "bottom", "topright", -5)
    t.order:Color(0, 125, 255)
    t.order:IgnoreInput()
    t.order:Align("rightcenter")
    t.order:Font("font_clear_medium_bold")
    
    t.destro = e("Label")
    t.destro:SetText(L"100")
    t.destro:Resize(65)
    t.destro:AnchorTo(t, "bottom", "topleft", 5)
    t.destro:Color(255, 0, 0)
    t.destro:IgnoreInput()
    t.destro:Align("leftcenter")
    t.destro:Font("font_clear_medium_bold")
    
    return t
end

function RealmStatus.Loading()
    if firstLoad then
		firstLoad = false
		
		if LibSlash ~= nil then
			LibSlash.RegisterWSlashCmd("rs", RealmStatus.Show)
			LibSlash.RegisterWSlashCmd("realmstatus", RealmStatus.Show)
			LibSlash.RegisterWSlashCmd("rsh", RealmStatus.ToggleShowingHistoryWindow)
		end
		
		if RealmStatusClock ~= nil then
			RealmStatusClock.Load()
		end
		
        if ( EA_Window_ZoneControl ~= nil ) and ( WindowGetShowing("EA_Window_ZoneControl") == true ) then
			WindowRegisterCoreEventHandler("EA_Window_ZoneControl", "OnLButtonUp", "RealmStatus.ToggleShowing")
			WindowRegisterCoreEventHandler("EA_Window_ZoneControl", "OnRButtonUp", "RealmStatus.ToggleShowingHistoryWindow")
        end
        
        LabelSetText( "RealmStatusHistoryWindowTitleBarText", L"T4 VP's History" )
        
        -- tint rows
        for indx = 1,10 do
			if math.mod(indx, 2) ~= 0 then WindowSetAlpha("RealmStatusHistoryWindowListRow" .. indx, 0.1 )
			else WindowSetAlpha("RealmStatusHistoryWindowListRow" .. indx, 0 ) end
        end
        
        -- Set mode
        LabelSetText("RealmStatusHistoryWindowRadioButtonsCampaignMode1Label", L"All" )
        LabelSetText("RealmStatusHistoryWindowRadioButtonsCampaignMode2Label", L"DvG" )
        LabelSetText("RealmStatusHistoryWindowRadioButtonsCampaignMode3Label", L"EvC" )
        LabelSetText("RealmStatusHistoryWindowRadioButtonsCampaignMode4Label", L"HEvDE" )
        ButtonSetPressedFlag("RealmStatusHistoryWindowRadioButtonsCampaignMode1Button", true )
    end
    -- end firstLoad
    
    RealmStatus.UpdateVP()
end

function RealmStatus.Initialize()
    RealmStatus.MakeWin()
    W:Hide()
    RealmStatus.MakeNotify()
    WN:Hide()
    RegisterEventHandler( SystemData.Events.CAMPAIGN_ZONE_UPDATED,  "RealmStatus.UpdateVP")
    RegisterEventHandler( SystemData.Events.LOADING_END,  "RealmStatus.Loading")
    RegisterEventHandler( SystemData.Events.RELOAD_INTERFACE,  "RealmStatus.Loading")
    
    RealmStatus.delay = 10
    historyTimeCount = RealmStatus.delay
end